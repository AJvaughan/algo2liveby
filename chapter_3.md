# Sorting

* Danny Hillis: MIT undergrad  
* Horrified by his roomate's sock matching ability  
* He would just throw the sock back in if it didn't match  
  
* Sorting is what computers do tho  
* In the late 19th century, there was population growth  
* The census barely got done  
* Hermon Hollerith was inspired by railroad tickets  
* Created a system of punched cards and a machine to sort them (the Hollerith Machine)  
* Government adopted it  
* Hollerith's firm became the Computing Tabulating Recording Company in 1911  
* renamed a few years later to International Business Machines (IBM)  
  &nbsp;
* First code ever written for a "stored program" computer was for sorting  
* By the 1960's, more than a quarter of computing resources were being spent on sorting  
  &nbsp;
* An important insight into sorting is that there more there is the harder it gets  
  &nbsp;
* In computer science, there is a shorthand for measuring algorithmic worst case scenarios.
* It's called the Big O Notation
* It has a quirk 
* It shows the relationship between the size of the problem and the problem's runtime  
    * Example:
    * Maid will have the same amt of house to clean regardless of guests: O(1)
    * Pass the roast (double people double time): O(n)
    * Amt of hugs at a party (1st person gets one second person hugs two and so on) : O(nˆ2)
  &nbsp;
* another sorting technique (a not very liked one) is called bubble sort
* It is quadratic
* It is basically like if you sorted a bookshelf by switching two at a time and restarting every time.
* It would mean your taking 25 times as long to sort 5 stacks.
  &nbsp;
* Another sorting technique is called the insertion sort
* You take all the books off the bookshelves and insert them in the right spot one by one.
* It's reputtion is beter but it's not much faster than the dreaded bubble sort
  &nbsp;
* So is there a way to get a faster sort?  
* Well, we know linear O(n) and constant O(1) won't work  
* So the algorithm would have to lie between n and nxn  
  &nbsp;
* In 1936 IBM created a line of machines called collators
* They would sort 2 piles into one - in linear time
  &nbsp;
* The program made to demonstrate the power of the stored program computer (made by John Von Neumann - 1945) was the one to take this idea of collating and figure it out
* You just put the smaller value on top (out of two packs mergig into one.)
* This was called MergeSort
* Legendary
  &nbsp;
* The Preston Library is one of the best book sorters in the world
* sorts in linear time
* They use the bucket sort method
* Because if you want to sort n objects into m buckets the equation looks like this: O(nm)
  &nbsp;
* The big thing to ask yourself is if you actually need to sort. Would it take more time to search or sort?
  &nbsp;
* Charles Lutwidge Dodgson (Lewis Carroll) (was a mathmatical lecturer at oxford) pointed out that silver is a lie in matches where a single elimination means tournament elimination
* Technically the true second best could be any of the players eliminated by the best.
* Round Robin Tournaments - where everyone fights everyone - is quadratic
* The ladder method - where they fight the people better than them and if they win they switch places - is also quadratic
* Not only with silver but we need to be aware of the gold.
* math shows that there is very little chance that the actual best team will win.
* Scientist calls this phenomonon noise
* The best algorithm in face of noise is called the comparison counting sort
* each item is compared with all the others, generating a tally of how many items it is bigger than. 
* It is quadratic which means it is not a popular choice but it is pretty fault tolerant.
  &nbsp;
* All these examples have been ordering so that the smallest is put on top
* what if it happens the other way around
* it would look a bit like online poker
* You sit down at a table and if someone better sits down you scram
* ^That is called displacement
* It is using your knowledge of the heiarchy to determine when a confrontation isn't worth it (neumann)
* if the heiarchy doesn't exist there's more problems
  &nbsp;
* There are ways of making the order without the costs
* like marathon runners who only endure one race
* This is a move from ordinal numbers (ranks) to cardinal numbers (assigns a measure to something's caliber)
  &nbsp;
* unlike chickens - determining hierchy doesn't have to be bloody.
* The bigger fish is the dominant fish
* We need to work in the cardinal
* And even though its a rat race at least its a race and not a fight